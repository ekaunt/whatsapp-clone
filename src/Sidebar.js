import React, { useEffect, useState } from 'react'
import "./Sidebar.css"
import {Avatar, IconButton} from "@material-ui/core"
import { SearchOutlined, MoreVert, Chat, DonutLarge } from '@material-ui/icons'
import SidebarChat from './SidebarChat'
import db from "./firebase"
import { collection, onSnapshot } from 'firebase/firestore'

function Sidebar() {
	const [rooms, setRooms] = useState([]);

	useEffect(() => {
		const unsubscribe = onSnapshot(collection(db, 'rooms'), snapshot => {
			setRooms(snapshot.docs.map(doc => ({
				id: doc.id,
				data: doc.data(),
			})))
		})

		return () => {
			unsubscribe();
		}
	}, [])

	return (
		<div className="sidebar">
			<div className="sidebar-header">
				<Avatar />
				<div className="sidebar-header-right">
					<IconButton>
						<DonutLarge />
					</IconButton>
					<IconButton>
						<Chat />
					</IconButton>
					<IconButton>
						<MoreVert />
					</IconButton>
				</div>
			</div>

			<div className="sidebar-search">
				<div className="sidebar-search-container">
					<SearchOutlined />
					<input placeholder="Search or start new chat" type="text" />
				</div>
			</div>

			<div className="sidebar-chats">
				<SidebarChat addNewChat />
				{rooms.map(room => (
					<SidebarChat key={room.id} id={room.id} name={room.data.name} />
				))}
			</div>
		</div>
	)
}

export default Sidebar