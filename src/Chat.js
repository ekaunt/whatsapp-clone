import { Avatar, IconButton } from '@material-ui/core'
import { AttachFile, InsertEmoticon, Mic, MoreVert, SearchOutlined } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import "./Chat.css"
import axios from './axios'
import db from './firebase'
import { addDoc, collection, doc, onSnapshot, orderBy, query, serverTimestamp } from 'firebase/firestore'
import { useStateValue } from './StateProvider'


function Chat() {
	const [input, setInput] = useState("")
	const [seed, setSeed] = useState('');
	const { roomId } = useParams();
	const [roomName, setRoomName] = useState("")
	const [messages, setMessages] = useState([])
  const [{user}, dispatch] = useStateValue();

	useEffect(() => {
		if(roomId) {
			onSnapshot(doc(db, "rooms", roomId), snapshot => (
				setRoomName(snapshot.data().name)
			))

			const q = query(collection(db, "rooms", roomId, "messages"), orderBy('timestamp', 'asc'))
			onSnapshot(q, (snapshot) => (
				setMessages(snapshot.docs.map(doc => doc.data()))
			))
		}
	}, [roomId])

	useEffect(() => {
		setSeed(Math.floor(Math.random() * 5000))
	}, [roomId])
		
	const sendMessage = async (e) => {
		e.preventDefault()

		addDoc(collection(db, "rooms", roomId, "messages"), {
			message: input,
			name: user.email,
			timestamp: serverTimestamp(),
		})

		setInput("")
	}

	return (
		<div className="chat">
			<div className="chat-bg"></div>
			<div className="chat-header">
				<Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />

				<div className="chat-header-info">
					<h3>{roomName}</h3>
					<p>Last seen{" "}{new Date(messages[messages.length-1]?.timestamp?.toDate()).toUTCString()}</p> 
				</div>

				<div className="chat-header-right">
					<IconButton>
						<SearchOutlined />
					</IconButton>
					<IconButton>
						<AttachFile />
					</IconButton>
					<IconButton>
						<MoreVert />
					</IconButton>
				</div>
			</div>

			<div className="chat-body">
				{messages.map(message => (
					<p className={`chat-message ${message.name === user.email && "chat-receiver"}`}>
					<span className="chat-name">{message.name}</span>
					{message.message}
					<span className="chat-timestamp">{new Date(message.timestamp?.toDate()).toUTCString()}</span>
					</p>
				))}
			</div>
			
			<div className="chat-footer">
				<InsertEmoticon />
				<form>
					<input value={input} onChange={(e) => setInput(e.target.value)} placeholder="Type a message..." type="text" />
					<button onClick={sendMessage} type="submit">Send a Message</button>
				</form>
				<Mic />
			</div>
		</div>
	)
}

export default Chat
