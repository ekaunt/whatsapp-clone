import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyDaFhgE0pXKTPMm9VOzF4SIO8E73EQoi1Q",
  authDomain: "whatsapp-clone-aeed8.firebaseapp.com",
  projectId: "whatsapp-clone-aeed8",
  storageBucket: "whatsapp-clone-aeed8.appspot.com",
  messagingSenderId: "1077774284103",
  appId: "1:1077774284103:web:7234d000216cd1da330862"
};

  
  const firebaseApp = initializeApp(firebaseConfig);
  const db = getFirestore(firebaseApp);
  const auth = getAuth(firebaseApp);

  export { auth };
  export default db;