import { Avatar } from '@material-ui/core'
import React, { useEffect, useState } from 'react'
import "./SidebarChat.css"
import db from './firebase'
import { collection, addDoc, query, orderBy, onSnapshot } from "firebase/firestore"
import { Link } from 'react-router-dom'

function SidebarChat({ id, name, addNewChat}) {
	const [seed, setSeed] = useState('');
	const [messages, setMessages] = useState("")

	useEffect(() => {
		if (id) {
			const q = query(collection(db, "rooms", id, "messages"), orderBy('timestamp', 'desc'))
			onSnapshot(q, snapshot => (
				setMessages(snapshot.docs.map((doc) => doc.data()))
			))
		}
	}, [id])

	useEffect(() => {
		setSeed(Math.floor(Math.random() * 5000))
	}, [])

	const createChat = () => {
		const roomName = prompt("Please enter name for chat");
		if (roomName) {
			addDoc(collection(db, "rooms"), {
				name: roomName,
			})
		}
	}

	return !addNewChat ? (
		<Link to={`/rooms/${id}`}>
			<div className="sidebar-chat">
				<Avatar src={`https://avatars.dicebear.com/api/human/${seed}.svg`} />
				<div className="sidebar-chat-info">
					<h2>{name}</h2>
					<p>{messages[0]?.message}</p>
				</div>
			</div>
		</Link>
	) : (
		<div onClick={createChat} className="sidebar-chat">
			<h2>Add New Chat</h2>
		</div>
	)
}

export default SidebarChat
