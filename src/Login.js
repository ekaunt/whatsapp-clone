import React from 'react'
import "./Login.css"
import whatsapp from "./whatsapp.png"
import { Button } from '@material-ui/core'
import { auth } from './firebase'
import { createUserWithEmailAndPassword, signInWithEmailAndPassword } from 'firebase/auth'
import { useStateValue } from './StateProvider'
import { actionTypes } from './reducer'

function Login() {
	const [{}, dispatch] = useStateValue();

	const signIn = () => {
		const email = prompt("Enter email")
		const password = prompt("Enter password (6 chars min, will show)")
		signInWithEmailAndPassword(auth, email, password)
		.then((result) => {
			dispatch({
				type: actionTypes.SET_USER,
				user: result.user,
			})
		}).catch((error) => {
			alert(error.message)
		})
	}

	const signUp = () => {
		const email = prompt("Enter email")
		const password = prompt("Enter password (6 chars min, will show)")
		createUserWithEmailAndPassword(auth, email, password)
		.then((result) => {
			console.log("signed up "+result.user)
		}).catch((error) => {
			alert(error.message)
		})
	}
	
	return (
		<div className="login">
			<div className="login-container">
				<img src={whatsapp} alt="" />
				<div className="login-text">
					<h1>Sign in to WhatsApp</h1>
				</div>

				<Button className="login-signin" onClick={signIn}>
					Sign In
				</Button>
				<Button className="login-signup" onClick={signUp}>
					Sign Up
				</Button>
			</div>
		</div>
	)
}

export default Login
